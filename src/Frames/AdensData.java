/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;

import org.apache.commons.lang.StringUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;

/**
 *
 * @author Anna
 */
public class AdensData extends javax.swing.JFrame {

    Frame1 frame1 = null;
    DefaultListModel<String> model = new DefaultListModel();
    DefaultListModel<String> model2 = new DefaultListModel();
    String selectedNuclide = "";
    HashMap<String, String> about_nuclides = null;
    ArrayList<String> nuclides = new ArrayList<>();
     Properties p = new Properties();
    InputStream fs = null;
    static Logger log = Logger.getLogger(AdensTable.class.getName());

    /**
     * Creates new form adensTable
     */
    public AdensData(Frame1 frame1, String fuelname) {
        this.frame1 = frame1;
        initComponents();
        this.getPropertyInformation();
        jList1.setModel(model);
        jList2.setModel(model2);
        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        this.getNuclideList(fuelname);
        setLocationRelativeTo(null);

    }

    public void setFuel(String fuelname) {
        jLabel2.setText(fuelname);
    }

        
    public void getPropertyInformation(){
        try {
            fs = getClass().getClassLoader().getResourceAsStream("serpent_analizator.properties");
            if (fs == null) {
                log.log(Level.INFO, "[getPropertyInformation] fs is null");
            } else {
                p.load(fs);
            }
        } catch (Exception e) {
            log.log(Level.INFO, "[getPropertyInformation] - ImputStream for info.properties: " + e.getMessage());
        }
    }
    
    public ArrayList<String> getNuclideList(String fuelname) {
        try {
            /*ФАЙЛ УКАЗАТЬ, НЕ ЗАБЫТЬ!*/
 /*String pythonScriptPath = "C:\\Users\\Anna\\PycharmProjects\\SearchInformation\\adensGetter.py";
            String[] cmd = {"python", pythonScriptPath};*/

            StringBuffer fileName = new StringBuffer(p.getProperty("pathFuelFile_1"));
            fileName.append(fuelname).append(p.getProperty("pathFuelFile_2"));
            System.out.println("Название файла: " + fileName.toString());

            /*читаем текстовый файл для определенного fuel*/
            FileInputStream inputStream = null;
            File adensFile = new File(fileName.toString());
            inputStream = new FileInputStream(adensFile);
            Scanner sc = new Scanner(inputStream, "UTF-8");

            //ArrayList<String> nuclides = new ArrayList<>(); //список нуклидов
            about_nuclides = new HashMap<String, String>();

            /*считываем файл построчно*/
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (line.isEmpty()) {
                    continue;
                }
                line.trim();
                String[] line_parts = StringUtils.splitPreserveAllTokens(line, "%");
                nuclides.add(line_parts[1].trim());
                about_nuclides.put(line_parts[1].trim(), line_parts[0].trim());
            }

            /*выводим все нуклиды для определенного fuel*/
            for (String nuclide : nuclides) {
                model.addElement(nuclide);
            }
            if ((inputStream != null) && (sc != null)) {
                sc.close();
                inputStream.close();
            }

        } catch (Exception e) {
            e.getMessage();
        }
        return nuclides;
      //  return about_nuclides;
    }

    public void getNuclideInformation(String nuclide) {
        if (about_nuclides == null) {
            return;
        } else if (nuclide != null) {
            if (nuclide.length() != 0) {
                try {
                    for (Map.Entry entry : about_nuclides.entrySet()) {
                        String neededNuclide = (String) entry.getKey();
                        if (neededNuclide.equals(nuclide)) {
                            String value = (String) entry.getValue();
                            String[] details = StringUtils.splitPreserveAllTokens(value.trim(), " ");
                            int count_of_details = details.length;
                            for (int i = 0; i < count_of_details - 1; i++) {
                                model2.addElement(details[i]);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.getMessage();
                }

            }
        }
    }

    public void deleteNuclide(String nuclide) {
        try {
            if (nuclide != null) {
                if (nuclide.length() != 0) {
                    if (nuclides != null) {
                        if (nuclides.size() != 0) {
                            System.out.println("заше");
                            System.out.println("before: "+ nuclides.size());
                            nuclides.remove(nuclide);
                            System.out.println("after:" + nuclides.size());
                            
                            model.clear();
                            for (String n : nuclides) {
                                model.addElement(n);
                            }
                        } else {
                            System.out.println("Массив нуклидов пуст");
                        }
                    } else {
                        System.out.println("Список нуклидов - null");
                    }
                } else {
                    System.out.println("Нуклид не выбран");
                }
            } else {
                System.out.println("Выбранный нуклид - null");
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jButton2 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        jLabel3 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Просмотр данных: ");

        jLabel2.setText("jLabel2");
        jLabel2.setMaximumSize(new java.awt.Dimension(34, 24));

        jButton1.setText("Назад");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(jList1);

        jButton2.setText("Изменение концентраций");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jList2.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(jList2);

        jLabel3.setText("Список нуклидов");

        jButton3.setText("Удалить нуклид");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton3)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel3)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButton1))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // возврат к 1му(со списком fuel) фрейму
        if (frame1 == null) {
            frame1 = new Frame1();
        }
        this.setVisible(false);
        frame1.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        /*кнопка для просмотра концентраций для нуклида*/
        selectedNuclide = jList1.getSelectedValue();
        this.getNuclideInformation(selectedNuclide);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList1ValueChanged
        /**/
        jButton2.setEnabled(true);
        jButton3.setEnabled(true);
    }//GEN-LAST:event_jList1ValueChanged

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        //вызов метода удаления нуклида из списка нуклидов
        this.deleteNuclide(jList1.getSelectedValue());
    }//GEN-LAST:event_jButton3ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JList<String> jList1;
    private javax.swing.JList<String> jList2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    // End of variables declaration//GEN-END:variables
}
