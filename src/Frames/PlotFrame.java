/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;

import java.util.ArrayList;
import javax.swing.JFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.LogAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Anna
 */
public class PlotFrame extends javax.swing.JFrame {

    AdensTable table = null;
    JFrame frame = null;

    /**
     * Creates new form PlotFrame
     */
    public PlotFrame(AdensTable table, ArrayList<String> column, String fuelname, String nuclide) {
        initComponents();
        this.setFuel(fuelname);
        this.setNuclide(nuclide);
        setLocationRelativeTo(null);

        // super(title);
        if (column.size() == 0) {
            System.out.println("ArrayList<String> column = 0");
        }
        try {
            ArrayList<Double> data = new ArrayList<>();
            try {
                for (String col : column) {
                    data.add(Double.parseDouble(col));
                }
            } catch (Exception e) {
                e.getMessage();
                e.printStackTrace();
            }

            XYSeriesCollection dataset = new XYSeriesCollection();
            XYSeries series1 = new XYSeries("Series 1");

            for (int i = 0; i < data.size() - 1; i++) {
            //    System.out.println("x = " + i + ", y = " + data.get(i));
                series1.add(i, data.get(i));
            }
            dataset.addSeries(series1);

            LogAxis yAxis = new LogAxis("Y");
            yAxis.setBase(2);
            yAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

            NumberAxis xAxis = new NumberAxis("X");
            xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

            /*  JFreeChart chart = ChartFactory.createScatterPlot(
                    "Диаграмма выбранных начальных условий", // title
                   // "X", "Y", // axis labels
                    "X", yAxis,
                    dataset, // dataset
                    PlotOrientation.VERTICAL,
                    true, // legend? yes
                    true, // tooltips? yes
                    false // URLs? no
            );

            ChartPanel chartPanel = new ChartPanel(chart);
            chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
            setContentPane(chartPanel);*/
            XYPlot plot = new XYPlot(new XYSeriesCollection(series1),
                    xAxis, yAxis, new XYLineAndShapeRenderer(true, false));
            JFreeChart chart = new JFreeChart(
                    "Динамика концентрации нуклида", JFreeChart.DEFAULT_TITLE_FONT, plot, false);
            /*JFrame */frame = new JFrame("Диаграмма выбранной начальной концентрации");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setContentPane(new ChartPanel(chart));
            frame.pack();
            frame.setVisible(true);
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }

    public void setFuel(String fuelname) {
        jLabel1.setText(fuelname);
    }
    public void setNuclide(String nuclide){
        jLabel4.setText(nuclide);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("Назад");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("jLabel1");

        jLabel2.setText("Концентрация:");

        jLabel3.setText("Выбранный нуклид:");

        jLabel4.setText("jLabel4");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(26, 26, 26)
                        .addComponent(jLabel1)
                        .addGap(0, 243, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        /*кнопка назад*/
        this.setVisible(false);
        if (table == null) {
            table = new AdensTable(null, jLabel1.getText());
        }
        this.setVisible(false);
        frame.setVisible(false);
        table.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    // End of variables declaration//GEN-END:variables
}
