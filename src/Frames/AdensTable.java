/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Frames;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Anna
 */
public class AdensTable extends javax.swing.JFrame {

    Frame1 frame1 = null;
    DefaultTableModel adensModel = null;
    Vector<Vector<String>> main_vector = null;
    Vector<String> nameColumns = null;
    ListSelectionModel lsm = null;
    ArrayList<String> nuclide_list = null;
    int row_nuclide = 0;

    Properties p = new Properties();
    InputStream fs = null;
    static Logger log = Logger.getLogger(AdensTable.class.getName());

    /**
     * Creates new form AdensTable
     */
    public AdensTable(Frame1 frame1, String fuelname) {
        this.frame1 = frame1;
        initComponents();
        this.getPropertyInformation();
        /*формируем начальную таблицу из файла*/
        this.getAdensDataTable(fuelname);
        adensModel = new DefaultTableModel(main_vector, nameColumns);
        jTable1.setModel(adensModel);
        setLocationRelativeTo(null);

    }

    public void setFuel(String fuelname) {
        jLabel2.setText(fuelname);
    }

    public void getPropertyInformation() {
        try {
            fs = getClass().getClassLoader().getResourceAsStream("serpent_analizator.properties");
            if (fs == null) {
                log.log(Level.INFO, "[getPropertyInformation] fs is null");
            } else {
                p.load(fs);
            }
        } catch (Exception e) {
            log.log(Level.INFO, "[getPropertyInformation] - ImputStream for info.properties: " + e.getMessage());
        }
    }

    public void getAdensDataTable(String fuelname) {
        try {
            main_vector = new Vector<>();
            nameColumns = new Vector<>();
            nuclide_list = new ArrayList<>();
            StringBuffer fileName = new StringBuffer(p.getProperty("pathFuelFile_1"));
            fileName.append(fuelname).append(p.getProperty("pathFuelFile_2"));
            System.out.println("Название файла для таблицы: " + fileName.toString());

            /*читаем текстовый файл для определенного fuel*/
            FileInputStream inputStream = null;
            File adensFile = new File(fileName.toString());
            inputStream = new FileInputStream(adensFile);
            Scanner sc = new Scanner(inputStream, "UTF-8");

            int colNum = 0;
            /*считываем файл построчно*/
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (line.isEmpty()) {
                    continue;
                }
                line.trim();
                String[] line_parts = StringUtils.splitPreserveAllTokens(line, "%");

                String nuclides_in_file = line_parts[1];
               // for (int k = 0; k < nuclides_in_file.length - 1; k++) {
                    nuclide_list.add(nuclides_in_file);
              //  }
                String[] columns = StringUtils.splitPreserveAllTokens(line_parts[0], " ");
                int number_of_col = columns.length;
                Vector<String> vector = new Vector<>();
                colNum = 0;
                for (int i = 0; i < columns.length - 1; i++) {
                    vector.add(columns[i]);
                    /* Double d = new Double(columns[i]);
                    String str = String.format("%6.3E", d);
                    vector.add(str);*/
                    colNum++;
                }
                main_vector.add(vector);
            }
            Integer k = 0;
            for (int j = 0; j < colNum - 1; j++) {
                k = j;
                nameColumns.add(k.toString());
            }
            if (sc != null) {
                sc.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }

        } catch (Exception e) {
            e.getMessage();
        }
    }

    /*метод выделяет из таблицы колонку (для формирования входного файла)*/
    public ArrayList<String> /*int */ getColumn() {
        lsm = jTable1.getColumnModel().getSelectionModel();
        ArrayList<String> selectedColumn = null;
        int col = jTable1.getSelectedColumn();
        System.out.println("selected col = " + col);

        if (adensModel == null) {
            //  return null;

        } else {
            int row = adensModel.getRowCount();
            try {
                selectedColumn = new ArrayList<>();
                // TableColumn tableColumn = jTable1.getColumnModel().getColumn(col);
                for (int i = 0; i < row - 1; i++) {
                    selectedColumn.add((String) adensModel.getValueAt(i, col));
                    /* Double d = Double.parseDouble((String) adensModel.getValueAt(i, col));
                    selectedColumn.add(d.toString());*/
                }

            } catch (Exception e) {
                e.getMessage();
            }

        }
        return selectedColumn;
    }

    public ArrayList<String> getRow() {
        ArrayList<String> selectedRow = null;
        int row = jTable1.getSelectedRow();
        row_nuclide = row;
        if (adensModel == null) {
            //  return null;

        } else {
            int col = adensModel.getColumnCount();
            try {
                selectedRow = new ArrayList<>();
                for (int i = 0; i < col - 1; i++) {
                    selectedRow.add((String) adensModel.getValueAt(row, i));

                }

            } catch (Exception e) {
                e.getMessage();
            }

        }

        return selectedRow;
    }

    /*метод удаляет колонку из таблицы. !*/
    public void deleteColumn(int col) {
        try {
            nameColumns = new Vector<String>();
            for (int j = 0; j < jTable1.getColumnCount(); j++)//Запись колонок в вектор
            {
                nameColumns.add(jTable1.getColumnName(j));
            }
            Integer coll = new Integer(col);
            Vector<Vector<String>> buffer_data = new Vector<Vector<String>>(adensModel.getDataVector());//Запись данных с таблицу
            for (int j = 0; j < jTable1.getColumnCount(); j++) {
                if (nameColumns.get(j).equals(coll.toString())) {
                    nameColumns.removeElementAt(j);
                    for (int i = 0; i < buffer_data.size(); i++) {
                        buffer_data.get(i).removeElementAt(j);
                    }
                    break;
                }
            }
            adensModel = new DefaultTableModel(buffer_data, nameColumns);
            jTable1.setModel(adensModel);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public Double summator(ArrayList<String> column) {
        Double sum = 0.0;
        for (String s : column) {
            Double d = Double.parseDouble(s);
            sum = sum + d;
        }
        return sum;
    }

    public void insert_in_file(ArrayList<String> column, String filename1, Double sum) {
        if (column == null) {
            log.log(Level.INFO, "[insert_in_file] column is null");
            return;
        } else if (column.size() == 0) {
            log.log(Level.INFO, "[insert_in_file] column is empty");
        } else {
            try {
                AdensData adata = new AdensData(frame1, jLabel2.getText());
                System.out.println("jLabel2.getText()=" + jLabel2.getText());
                ArrayList<String> nuclides = adata.getNuclideList(filename1);
                String summa = String.format("%6.3E", sum);
                double vol = 0.0;
                String volstr = "vol";
                //double sum = 0.0;
                StringBuffer strBuf = new StringBuffer("mat ");
                strBuf.append(jLabel2.getText());
                String substr = strBuf.toString();
                strBuf.append("    ").append(summa).append("    ").append(volstr)
                        .append(" ").append(vol).append("    burn 1");
                strBuf.append(System.getProperty("line.separator"));
                System.out.println("substr = " + substr);

                /*считываем старый тхт*/
                BufferedReader reader = new BufferedReader(new FileReader(p.getProperty("pathtoTXT")));
                String line = null;
                StringBuilder stringBuilder = new StringBuilder();
                String ls = System.getProperty("line.separator");
                int pos = 0;
                int start = 0;
                int end = 0;
                boolean flag = false;

                /*А писать будем в этот файл*/
                File txtfilenew = new File(filename1);
                FileWriter fw = new FileWriter(txtfilenew/*, true*/);

                /*читаем старый тхт файл и одновременно пишем в новый*/
                while ((line = reader.readLine()) != null) {
                    pos++;
                    // System.out.println("pos = "+pos);
                    if (line.contains(substr.concat(" "))) {
                        System.out.println("is in start, pos = " + pos);
                        start = pos;
                        flag = true;
                        continue;
                    }
                    if ((flag == true) && (line.contains("fuel"))) {
                        System.out.println("is in end, pos = " + pos);
                        end = pos;
                        flag = false;
                    }
                    if (flag == false) {
                        fw.write(line);
                        fw.write(System.lineSeparator());
                    }
                }

                System.out.println("start=" + start);
                System.out.println("end=" + end);

                for (int i = 0; i < nuclides.size() - 1; i++) {
                    if ((nuclides.get(i).equals("total") || (nuclides.get(i).equals("lost data")))) {

                    } else {
                        strBuf.append("      ").append(nuclides.get(i).substring(0, nuclides.get(i).length() - 1));
                        strBuf.append(".03c").append(" ").append(column.get(i));
                        strBuf.append(System.getProperty("line.separator"));
                    }
                }
                System.out.println(strBuf.toString());
                fw.write(strBuf.toString());

                // sc.close();
                // inputStream.close();
                reader.close();
                fw.close();

            } catch (Exception e) {
                e.printStackTrace();
                e.getMessage();
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Изменения в составе:");

        jLabel2.setText("jLabel2");

        jButton1.setText("Назад");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.addHierarchyListener(new java.awt.event.HierarchyListener() {
            public void hierarchyChanged(java.awt.event.HierarchyEvent evt) {
                jTable1HierarchyChanged(evt);
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jButton3.setText("Удалить столбец");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Сделать начальным условием");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton2.setText("Изобразить");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 804, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2)
                        .addGap(35, 35, 35)
                        .addComponent(jButton4)
                        .addGap(30, 30, 30)
                        .addComponent(jButton3)
                        .addGap(33, 33, 33)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jButton1)
                    .addComponent(jButton3)
                    .addComponent(jButton4)
                    .addComponent(jButton2))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // возврат к 1му(со списком fuel) фрейму
        if (frame1 == null) {
            frame1 = new Frame1();
        }
        this.setVisible(false);
        frame1.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked

    }//GEN-LAST:event_jTable1MouseClicked

    private void jTable1HierarchyChanged(java.awt.event.HierarchyEvent evt) {//GEN-FIRST:event_jTable1HierarchyChanged

    }//GEN-LAST:event_jTable1HierarchyChanged

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        int col = jTable1.getSelectedColumn();
        this.deleteColumn(col);
        System.out.println(col);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        /*вставляем в качестве начальных условий*/
        ArrayList<String> selectdColumn = this.getColumn();
        Double sum = 0.0;
        sum = this.summator(selectdColumn);
        this.insert_in_file(selectdColumn, "D:\\for_nir\\VVER-Caz_new.txt", sum);

    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        /*кнопка вызывает фрейм с графиком*/
        ArrayList<String> selectedRow = this.getRow();
        /*надо строку*/

        String nuclide = nuclide_list.get(row_nuclide);
        PlotFrame pf = new PlotFrame(this, selectedRow, jLabel2.getText(), nuclide);
        pf.pack();
        this.setVisible(false);
        pf.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
